#!/usr/bin/env python3

import os
import re
import requests
import subprocess
import time

PROJECT_URL = "https://gitlab.wikimedia.org/api/v4/projects/repos%2freleng%2fdocker-gc"


def suggest_new_version(old_version):
    # Break the version out into components
    parts = old_version.split(".")
    # Add one to the minor version
    parts[1] = str(int(parts[1]) + 1)
    # Reset patch version to 0
    parts[2] = "0"
    # And mush back together
    return ".".join(parts)


def assert_clean_checkout():
    output = subprocess.check_output(["git", "status", "--porcelain"], text=True)
    if output:
        raise SystemExit(f"Checkout is not clean\n{output}")


def get_target_branch():
    return requests.get(PROJECT_URL).json()["default_branch"]


def rebase():
    subprocess.run(["git", "pull", "--rebase"], check=True)


def sanity_checks(target_branch):
    assert_clean_checkout()
    rebase()
    assert_clean_checkout()
    output = subprocess.check_output(
        ["git", "rev-list", f"origin/{target_branch}.."], text=True
    )
    if output:
        raise SystemExit(f"Local commits detected:\n{output}\nAborting")


def push_gitlab(vers, target_branch):
    user = os.getenv("USER")
    if not user:
        raise SystemExit("USER not set in environment")
    remote_branch = f"review/{user}/release-{vers}"

    p = subprocess.run(
        [
            "git",
            "push",
            "-o",
            "merge_request.create",
            "-o",
            f"merge_request.target={target_branch}",
            "-o",
            "merge_request.merge_when_pipeline_succeeds",
            "-o",
            "merge_request.remove_source_branch",
            "origin",
            f"HEAD:{remote_branch}",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        check=True,
        text=True,
    )

    m = re.search(r"/-/merge_requests/(\d+)", p.stdout)
    if not m:
        raise Exception(
            f"Unexpected output from git push to GitLab.  Merge request URL not found.\n{p.stdout}"
        )

    mr_id = m[1]

    print(f"Waiting for merge request {mr_id} to merge...", end="", flush=True)

    while True:
        print(".", end="", flush=True)
        r = requests.get(f"{PROJECT_URL}/merge_requests/{mr_id}")
        r.raise_for_status()
        r = r.json()

        state = r["state"]
        status = r["merge_status"]

        if state == "merged":
            print(f"\nMerge request {mr_id} has been merged")
            return
        if re.search("cannot", status):
            print()
            raise SystemExit(f"Merge request {mr_id} can't be merged")

        time.sleep(5)


def main():
    target_branch = get_target_branch()

    sanity_checks(target_branch)

    with open("VERSION") as f:
        old_version = f.readline()

    suggested_vers = suggest_new_version(old_version)
    print(f"Current version: {old_version}")
    vers = input(f"New version (default {suggested_vers}): ")
    if not vers:
        vers = suggested_vers

    print(f"Selected version {vers}")
    answer = input("Ready to proceed? ")
    if answer != "y":
        raise SystemExit("Aborted.")

    with open("VERSION", "w") as f:
        print(vers, file=f)

    subprocess.run(["git", "add", "VERSION"], check=True)
    subprocess.run(["git", "commit", "-m", f"Release {vers}"], check=True)

    print("Pushing commit")
    push_gitlab(vers, target_branch)
    rebase()
    assert_clean_checkout()

    subprocess.run(
        ["git", "tag", "--sign", "-m", f"Release {vers}", vers, "HEAD"], check=True
    )
    subprocess.run(["git", "push", "origin", vers], check=True)
    print(f"Release {vers} has been tagged")


if __name__ == "__main__":
    main()
