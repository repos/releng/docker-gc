#!/bin/bash

set -eu -o pipefail

docker run --rm --user root \
       -v /var/run/docker.sock:/var/run/docker.sock \
       -v docker-resource-monitor:/state \
       local/docker-gc:latest --state-file /state/state.json "$@"
