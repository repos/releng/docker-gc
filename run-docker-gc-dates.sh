#!/bin/bash

# Script analogous to run-docker-gc.sh, but calling the gc with `--use-creation-dates`

set -eu -o pipefail

docker run --rm --user root \
       -v /var/run/docker.sock:/var/run/docker.sock \
       local/docker-gc:latest --use-creation-dates "$@"
