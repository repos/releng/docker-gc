VARIANTS = resource-monitor docker-gc

%-image:
	DOCKER_BUILDKIT=1 docker build \
		-f .pipeline/blubber.yaml \
		--target $* \
		-t local/$* .

.PHONY: build-images
build-images: $(VARIANTS:%=%-image)

.PHONY: lint reformat
lint:
	tox -e lint
reformat:
	tox -e reformat


