#!/usr/bin/env python3

import argparse
import docker
import humanfriendly
import json
import logging
import re
import requests
import time
from datetime import datetime

Args = None


def parse_args():
    """Parses arguments and set the 'Args' global with the result"""
    ap = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)

    basic = ap.add_argument_group("Basic options")

    basic.add_argument(
        "-f",
        "--state-file",
        help="Read state from the specified file",
        metavar="FILE",
        default=None,
    )
    basic.add_argument(
        "--http",
        help="Retrieve state from the specified URL",
        metavar="URL",
        default=None,
    )

    basic.add_argument(
        "--interval",
        help="Perform a GC operation every INTERVAL seconds.  If not specified, perform a one-shot operation.",
        default=None,
        type=int,
    )

    basic.add_argument(
        "--use-creation-dates",
        help="Sort resources by creation date instead of last time of access. Older resources get reaped first",
        action="store_true",
    )

    basic.add_argument(
        "--timeout",
        metavar="TIMEOUT",
        help="Set the timeout for docker operations.",
        default=60,
        type=int,
    )

    wm = ap.add_argument_group(
        "Water marks",
        """
Pruning is triggered when the amount of storage used by the specified
type of resource exceeds HIGH.  Once triggered, resources are deleted
one by one until storage used by the specified type of resource
reaches LOW.

HIGH and LOW are specified in bytes, but you may use 'k', 'm', 'g',
etc suffixes to specify (base-10) units.

 Example:
  --images 20g:15g

    If the storage space used by images exceeds 20GB, delete eligible
    images until image storage use is reduced to 15GB.

""",
    )

    wm.add_argument(
        "--images",
        help="Prune images using the specified HIGH and LOW water marks",
        metavar="HIGH:LOW",
        default=None,
    )
    wm.add_argument(
        "--volumes",
        help="Prune volumes using the specified LOW and HIGH water marks",
        metavar="HIGH:LOW",
        default=None,
    )

    f = ap.add_argument_group(
        "Filters",
        """
FILTER syntax:
  FIELD==VALUE: True if the specified field matches VALUE exactly.
  FIELD=~REGEXP: True if the specified field matches REGEXP.

  FIELD may be:
    id         : The resource id.  For images, this is a value like
                 sha256:d9da418af538...  For volumes, this is the name
                 of the volume.
    name       : The resource name.  For images, this is any of the image
                 tags (in name:tag form).  For volumes, this is the name
                 of the volume.
    label:KEY  : The value of the label with the specified KEY.

 Example:
  Match all images:
  --image-filter name=~.*

""",
    )

    f.add_argument(
        "--image-filter",
        help="Only consider images matching FILTER for pruning",
        metavar="FILTER",
        default=None,
        action="append",
    )

    f.add_argument(
        "--volume-filter",
        help="Only consider volumes matching FILTER for pruning",
        metavar="FILTER",
        default=None,
        action="append",
    )

    global Args
    Args = ap.parse_args()

    if Args.use_creation_dates:
        logging.info(
            "Using resource creation date to determine order of removal (older goes first)"
        )
    elif (Args.state_file is None and Args.http is None) or (
        Args.state_file and Args.http
    ):
        raise SystemExit("Please use either --state-file or --http")

    def parse_watermarks(which, string):
        m = re.match(r"(.+):(.+)$", string)
        if m:
            return {
                "high": humanfriendly.parse_size(m.group(1)),
                "low": humanfriendly.parse_size(m.group(2)),
            }
        else:
            raise SystemExit("Invalid syntax for --{}: {}".format(which, string))

    Args.watermarks = dict()

    if Args.images:
        Args.watermarks["image"] = parse_watermarks("images", Args.images)
    if Args.volumes:
        Args.watermarks["volume"] = parse_watermarks("volumes", Args.volumes)

    # label Keys are alphanumeric strings which may contain periods (.) and hyphens (-).
    # xref: https://docs.docker.com/config/labels-custom-metadata/#key-format-recommendations

    def parse_resource_filter(string):
        m = re.match(r"(name|id|label:[0-9a-zA-Z.-]+)(==|=~)(.*)$", string)
        if m:
            (field, operator, value) = m.groups()

            if operator == "==":
                operator = EqualityOperator(field, value)
            elif operator == "=~":
                try:
                    operator = RegexpOperator(field, value)
                except Exception:
                    raise SystemExit("Invalid filter regexp: {!r}".format(value))
            else:
                raise Exception("This should never happen")

            return operator
        else:
            raise SystemExit("Invalid filter: {!r}".format(string))

    if Args.volume_filter:
        Args.volume_filter = list(map(parse_resource_filter, Args.volume_filter))
    if Args.image_filter:
        Args.image_filter = list(map(parse_resource_filter, Args.image_filter))

    if not Args.volume_filter and not Args.image_filter:
        raise SystemExit("No volume or image filters supplied.  No action can be taken")


# FIXME: Maybe these should be named "filter" instead of "operator"
class FilterOperator:
    def __init__(self, field, desired_value):
        self.field = field
        self.desired_value = desired_value


class EqualityOperator(FilterOperator):
    def run_operator(self, value):
        return value == self.desired_value


class RegexpOperator(FilterOperator):
    def __init__(self, field, desired_value):
        super().__init__(field, desired_value)
        self.regexp = re.compile(desired_value)

    def run_operator(self, value):
        return re.search(self.regexp, value)


DOCKER_CLIENT = None


def get_docker_client():
    global DOCKER_CLIENT

    if DOCKER_CLIENT is None:
        DOCKER_CLIENT = docker.from_env(timeout=Args.timeout)

    return DOCKER_CLIENT


class Resource:
    def in_use(self):
        return self.refcount > 0


class Image(Resource):
    def __init__(self, id, created, size, repotags, labels, containers):
        self.id = id
        self.created = created
        self.size = size
        self.repotags = repotags or []
        self.labels = labels or {}
        self.refcount = containers

    def get_field_values(self, field):
        if field == "id":
            return [self.id]
        if field == "name":
            return self.repotags
        return try_labels(self, field)

    def prune(self):
        get_docker_client().images.remove(self.id)


class Volume(Resource):
    def __init__(self, name, created, size, labels, refcount):
        self.id = name
        self.created = created
        self.size = size
        self.labels = labels or {}
        self.refcount = refcount

    def get_field_values(self, field):
        if field == "id" or field == "name":
            return [self.id]
        return try_labels(self, field)

    def prune(self):
        # FIXME: VolumeCollection class doesn't have remove()
        get_docker_client().volumes.get(self.id).remove()


def transform_df(df):
    def to_timestamp(date_time):
        if "Z" in date_time:
            # date in UTC, directly parseable
            python_parseable = date_time
        else:
            # Remove extra colon in the timezone portion of the date to conform to Python's timezone format
            # directive (%z)
            parts = date_time.rsplit(":", 1)
            python_parseable = parts[0] + parts[1]

        creation_datetime = datetime.strptime(python_parseable, "%Y-%m-%dT%H:%M:%S%z")
        return creation_datetime.timestamp()

    images = []
    volumes = []

    for entry in df["Images"]:
        images.append(
            Image(
                entry["Id"],
                entry["Created"],
                entry["Size"],
                entry["RepoTags"],
                entry["Labels"],
                entry["Containers"],
            )
        )
    for entry in df["Volumes"]:
        volumes.append(
            Volume(
                entry["Name"],
                to_timestamp(entry["CreatedAt"]),
                entry["UsageData"]["Size"],
                entry["Labels"],
                entry["UsageData"]["RefCount"],
            )
        )

    df["Images"] = images
    df["Volumes"] = volumes


def resilient_df() -> dict:
    err_count = 0
    max_errs = 3
    retry_delay = 30

    while True:
        try:
            return get_docker_client().df()
        except docker.errors.APIError as e:
            # Cripes
            if (
                getattr(e, "explanation", None)
                == "a disk usage operation is already running"
            ):
                time.sleep(1)
                continue
            # Something unexpected.
            raise e
        except requests.exceptions.ReadTimeout:
            err_count += 1
            if err_count >= max_errs:
                logging.error(
                    "Encountered {err_count} consecutive read timeout errors during 'df()'"
                )
                raise
            logging.error(
                "Read timeout during 'df()'.  Sleeping for {} seconds before retrying.".format(
                    retry_delay
                )
            )
            time.sleep(retry_delay)
            logging.error("Retrying df()")


def get_resources_space_used(df, type):
    if type == "image":
        # To get accurate image size info, we must re-query the Docker daemon. :-(
        return resilient_df()["LayersSize"]

    total = 0

    for resource in df[type.capitalize() + "s"]:
        total += resource.size

    return total


def get_state_from_state_file(filename) -> dict:
    with open(filename) as f:
        return json.load(f)


def get_state_from_http(url) -> dict:
    r = requests.get(url)
    r.raise_for_status()
    return r.json()


def get_state() -> dict:
    if Args.use_creation_dates:
        return {}

    if Args.http:
        return get_state_from_http(Args.http)
    elif Args.state_file:
        return get_state_from_state_file(Args.state_file)
    else:
        raise Exception("This should never happen")


def try_labels(resource, field):
    m = re.match(r"label:(.*)$", field)
    if m:
        label = m.group(1)
        labels = resource.labels
        if labels:
            value = labels.get(label, None)
            if value:
                return [value]
    # Nada
    return []


def get_filters_for_type(type):
    return getattr(Args, "{}_filter".format(type))


def resource_is_eligible(type, resource) -> bool:
    "Returns True if at least one operator passes"

    if resource.in_use():
        return False

    for operator in get_filters_for_type(type):
        for field_value in resource.get_field_values(operator.field):
            if operator.run_operator(field_value):
                return True

    return False


def get_resource_last_access(type, resource, state):
    # Resources that we don't have access time info for are first in line for pruning
    return state[type + "s"].get(resource.id, 0)


def get_eligible_resources(resources, type):
    """
    Returns a list of eligible resources names of the given type, starting with the least recently used
    """

    state = get_state()

    res = []

    for resource in resources:
        if resource_is_eligible(type, resource):
            if Args.use_creation_dates:
                resource.sort_key = resource.created
            else:
                resource.sort_key = get_resource_last_access(type, resource, state)
            res.append(resource)

    res.sort(key=lambda resource: resource.sort_key)

    return res


def prune_resource(resource) -> bool:
    logging.info("Pruning {}".format(resource.id))

    try:
        resource.prune()
    except docker.errors.APIError as e:
        logging.error("Failed to prune {}: {}".format(resource, e))
        return False

    logging.info("Pruned {}".format(resource.id))
    return True


def prune_eligible_resources_until(df, type, stop_condition) -> bool:
    """
    Prune eligible resources of the specified type until stop_condition is satisfied.
    Returns True if the stop condition has been satisfied.
    Returns False if we ran out of eligible resources before reaching the stop condition
    """
    resources = df[type.capitalize() + "s"]  # image => Images
    candidates = get_eligible_resources(resources, type)

    while not stop_condition():
        if candidates:
            resource = candidates.pop(0)

            if prune_resource(resource):
                resources.remove(resource)
        else:
            return False

    # Reach stop condition
    return True


def prune_resources(df, type, low_water):
    """
    Deletes eligible resources of the specified type (volume or image) until
    the low water mark has been reached, or until we run out of eligible resources.
    """

    # "volumes" or "images"
    types = type + "s"

    logging.info(
        "Attempting to prune {} down to {}".format(
            types, humanfriendly.format_size(low_water)
        )
    )

    if prune_eligible_resources_until(
        df, type, lambda: get_resources_space_used(df, type) <= low_water
    ):
        logging.info(
            "Reached low water mark of {}".format(humanfriendly.format_size(low_water))
        )
    else:
        logging.warning(
            "Ran out of eligible {} to delete before reaching the low water mark".format(
                types
            )
        )


def maybe_prune_resources(df, type):
    #  "volumes" or "images"
    types = type + "s"
    current_usage = get_resources_space_used(df, type)

    marks = Args.watermarks.get(type, None)
    if marks:
        (low_water_mark, high_water_mark) = marks["low"], marks["high"]
    else:
        (low_water_mark, high_water_mark) = 0, 0

    msg = "{} usage is {}".format(types, humanfriendly.format_size(current_usage))

    if high_water_mark:
        if current_usage > high_water_mark:
            logging.info(
                "{}, above high water mark of {}.  Need to prune.".format(
                    msg, humanfriendly.format_size(high_water_mark)
                )
            )
            prune_resources(df, type, low_water_mark)
        else:
            logging.info(
                "{}, below high water mark of {}.".format(
                    msg, humanfriendly.format_size(high_water_mark)
                )
            )
    else:
        logging.info(msg + ".  No high water mark set.")


def perform_pruning_round():
    logging.info("Running df()")
    df = resilient_df()
    transform_df(df)
    logging.info(
        "df() returned {} images, {} volumes".format(
            len(df["Images"]), len(df["Volumes"])
        )
    )

    for type in ("volume", "image"):
        maybe_prune_resources(df, type)


def main():
    logging.basicConfig(format="[%(asctime)-15s] %(message)s", level=logging.INFO)

    parse_args()

    try:
        while True:
            perform_pruning_round()

            if Args.interval:
                time.sleep(Args.interval)
            else:
                # One-shot mode
                return
    except KeyboardInterrupt:
        raise SystemExit("Interrupted")


if __name__ == "__main__":
    main()
