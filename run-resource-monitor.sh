#!/bin/bash

set -eu -o pipefail

docker run -d --rm --user root \
       --name docker-resource-access-monitor \
       -v /var/run/docker.sock:/var/run/docker.sock \
       -v docker-resource-monitor:/state \
       local/resource-monitor:latest --state-file /state/state.json
