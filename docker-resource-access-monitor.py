#!/usr/bin/env python3

import argparse
import docker
import json
import logging
import os.path
import re
import tempfile
import threading
import time

# TODO:
# * If a state file is supplied, read from it during startup if it exists.
#   (but prune obsolete info from it)

try:
    import flask
except ImportError:
    flask = None


def parse_args():
    """Parses and returns the arguments."""
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-f",
        "--state-file",
        help="""
        The path to an optional state file.
        If supplied, state will be saved into the file periodically and
        information found in the file will be read during startup.
        """,
        metavar="FILE",
        default=None,
    )

    if flask:
        ap.add_argument(
            "--http",
            help="Enable HTTP server on ADDR (default is 127.0.0.1) and PORT",
            metavar="[ADDR:]PORT",
            default=None,
        )

    args = ap.parse_args()

    if args.http:
        m = re.match(r"(([\d.]+):)?(\d+)$", args.http)
        if m:
            addr = m.group(2) or "127.0.0.1"
            port = m.group(3)

            args.http = (addr, port)
        else:
            raise SystemExit("Invalid --http parameter: {!r}".format(args.http))

    return args


resources = {
    "version": "1.0.0",  # Version of the format of this data
    "images": {},  # key is id, value is last access time (Unix time)
    "volumes": {},  # key is id, value is last access time (Unix time)
}
StateChanged = True
resources_lock = threading.Lock()  # Lock protecting these two variables


def access_resource_by_id(type, id, time):
    global StateChanged

    with resources_lock:
        resources[type][id] = time
        StateChanged = True


def remove_resource(type, id):
    global StateChanged

    with resources_lock:
        try:
            del resources[type][id]
            StateChanged = True
        except KeyError:
            # No big deal
            pass


def remove_image(id):
    logging.info("Image {} was deleted".format(id))
    remove_resource("images", id)


def remove_volume(id):
    logging.info("Volume {} was deleted".format(id))
    remove_resource("volumes", id)


def access_image(id, time):
    id = canonicalize_image_id(id)
    if id:
        logging.info("Image {} accessed".format(id))
        access_resource_by_id("images", id, time)


def access_volume(id, time):
    logging.info("Volume {} accessed".format(id))
    access_resource_by_id("volumes", id, time)


def canonicalize_image_id(thing):
    """Returns a string like sha256:b5f8a27eb8630a8852f420ca8fce405cd1da9b4d163a9161b9f8e04eeab1b69c"""

    if re.match(r"sha256:[0-9a-f]{64}$", thing):
        # Already in canonical format.
        return thing

    try:
        return docker.from_env().images.get(thing).id
    except docker.errors.APIError:
        # Assume that an exception means that the image disappeared
        return None


def monitor_events():
    logging.info("Monitoring events...")

    for event in docker.from_env().events(decode=True):
        # print(event)

        type = event["Type"]
        action = event["Action"]
        time = event["time"]

        if type == "volume":
            name = event["Actor"]["ID"]
            # noqa {'Type': 'volume', 'Action': 'create', 'Actor': {'ID': 'deleteme', 'Attributes': {'driver': 'local'}}, 'scope': 'local', 'time': 1634063160, 'timeNano': 1634063160985175541}
            # noqa {'Type': 'volume', 'Action': 'destroy', 'Actor': {'ID': 'deleteme', 'Attributes': {'driver': 'local'}}, 'scope': 'local', 'time': 1634063049, 'timeNano': 1634063049503864538}
            # noqa {'Type': 'volume', 'Action': 'mount', 'Actor': {'ID': 'deleteme', 'Attributes': {'container': '8a4f042c6a2ae1a981acf0c9e0d502fdc5368fcafb4ac5c8caa6f4912fa84f59', 'destination': '/vol', 'driver': 'local', 'propagation': '', 'read/write': 'true'}}, 'scope': 'local', 'time': 1634063177, 'timeNano': 1634063177857730483}
            # noqa {'Type': 'volume', 'Action': 'unmount', 'Actor': {'ID': 'deleteme', 'Attributes': {'container': '3f80f0836aa3e55efa60278a122d45f5450720556c2e8d8470447133b05f5b66', 'driver': 'local'}}, 'scope': 'local', 'time': 1634063357, 'timeNano': 1634063357798858758}

            if action == "mount":
                access_volume(name, time)
            elif action == "destroy":
                remove_volume(name)

        # noqa {'status': 'start', 'id': 'b29e52a19ffb1a17f6e50f9a1d26d064f11c7319cde37196b928f76ba499cfa9', 'from': 'sha256:9e1a64aca99cbc89e5cb9a9022d74cf756138a5e465045df1c5fcadf0c56dab3', 'Type': 'container', 'Action': 'start', 'Actor': {'ID': 'b29e52a19ffb1a17f6e50f9a1d26d064f11c7319cde37196b928f76ba499cfa9', 'Attributes': {'image': 'sha256:9e1a64aca99cbc89e5cb9a9022d74cf756138a5e465045df1c5fcadf0c56dab3', 'name': 'zealous_murdock'}}, 'scope': 'local', 'time': 1634063736, 'timeNano': 1634063736669075562}
        if type == "container" and action == "start":
            access_image(event["from"], time)

        # noqa {'status': 'delete', 'id': 'sha256:e116d2efa2ab6f7af3e077771fda81477a2bc8d5c5e98d60ad00bccec714f6b9', 'Type': 'image', 'Action': 'delete', 'Actor': {'ID': 'sha256:e116d2efa2ab6f7af3e077771fda81477a2bc8d5c5e98d60ad00bccec714f6b9', 'Attributes': {'name': 'sha256:e116d2efa2ab6f7af3e077771fda81477a2bc8d5c5e98d60ad00bccec714f6b9'}}, 'scope': 'local', 'time': 1634064255, 'timeNano': 1634064255012115185}
        if type == "image" and action == "delete":
            remove_image(event["id"])


def get_resources_repr(type=str, resetStateChanged=False):
    global StateChanged

    with resources_lock:
        if resetStateChanged:
            StateChanged = False

        if type is str:
            return json.dumps(resources, indent=4)

        if type is dict:
            return resources

        raise Exception("Unhandled value for 'type': {!r}".format(type))


def resource_exists(cli, type, id) -> bool:
    try:
        getattr(cli, type).get(id)
        return True
    except docker.errors.APIError:
        # Assume that an exception means that the resource no longer exists
        return False


def remove_obsolete_resources_from_state():
    global resources
    global StateChanged

    with resources_lock:
        cli = docker.from_env()

        for type in ("images", "volumes"):
            dict = resources.get(type)
            junk = []
            for id in dict.keys():
                if not resource_exists(cli, type, id):
                    logging.info("{} {} from state no longer exists".format(type, id))
                    junk.append(id)
            if junk:
                for id in junk:
                    del dict[id]
                StateChanged = True


def read_state_file(statefile):
    try:
        with open(statefile) as f:
            logging.info("Reading prior state from {!r}".format(statefile))
            try:
                state = json.load(f)
            except Exception as e:
                logging.warning(
                    "Failed to parse state from {!r}: {}\nIgnoring.".format(
                        statefile, e
                    )
                )
                return

            if not isinstance(state, dict):
                logging.warning(
                    "Unexpected data found in {!r}: Ignoring.".format(statefile)
                )
                return

            if state.get("version", None) != "1.0.0":
                logging.warning(
                    "Unsupported version in state file: {}.  Ignoring.".format(
                        statefile
                    )
                )
                return

            with resources_lock:
                global resources
                global StateChanged

                resources = state
                StateChanged = True

    except FileNotFoundError:
        # OK if the file doesn't exist yet
        pass


def write_state_file(statefile):
    with tempfile.NamedTemporaryFile(
        "w", dir=os.path.dirname(statefile), delete=False
    ) as tmp:
        tmpname = tmp.name
        tmp.write(get_resources_repr(resetStateChanged=True))

    os.rename(tmpname, statefile)


def state_file_writer(statefile):
    while True:
        if StateChanged:
            logging.info("state file writer updating {}".format(statefile))
            write_state_file(statefile)
        time.sleep(1)


def start_state_file_writer(statefile):
    thread = threading.Thread(
        target=state_file_writer,
        name="State File Writer",
        args=(statefile,),
        daemon=True,
    )
    thread.start()


def http_server(addr, port):
    app = flask.Flask(__name__)

    @app.route("/")
    def transmit_state():
        return flask.jsonify(get_resources_repr(type=dict))

    app.run(host=addr, port=port, load_dotenv=False)


def start_http_server(addr, port):
    thread = threading.Thread(
        target=http_server, name="HTTP server", args=(addr, port), daemon=True
    )
    thread.start()


def main():
    args = parse_args()

    logging.basicConfig(format="[%(asctime)-15s] %(message)s", level=logging.INFO)

    if args.state_file:
        read_state_file(args.state_file)
        remove_obsolete_resources_from_state()
        start_state_file_writer(args.state_file)
    if flask and args.http:
        start_http_server(*args.http)

    try:
        monitor_events()
    except KeyboardInterrupt:
        raise SystemExit("Interrupted")


if __name__ == "__main__":
    main()
