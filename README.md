# docker-gc

This repo implements a tool that is like "docker system prune", but it
* only deals with images and volumes
* is less drastic
  * support pruning down to a low water mark (not necessarily deleting everything)
  * considers recency-of-use in the pruning selection process

The tool consists of two components:

## docker-resource-access-monitor.py

docker-resource-access-monitor.py watches the docker events stream to
track when images and volumes are used.  This information can be
written to a state file and/or exposed via HTTP.  The information is
intended to be used by the docker-gc.py component.

## docker-gc.py

docker-gc.py scans image and volume disk usage and triggers a pruning
if the high water mark has been reached.  Pruning deletes eligible
resources one by one until the low water mark has been reached, or
until all eligible resources have been exhausted.

Eligible resources are pruned starting with the least recently used.
Alternatively, it is possible to pass the flag `--use-creation-dates`
to prune starting with older images; in this case, it is not
necessary to specify a state file.

Eligible resources are selected using one or more filters.  A filter
can perform exact or regexp matches on the id, name, or labels of a
resource.  Examples:

    # All volumes are eligible
    --volume-filter name=~.*

    # Images from a specific repository are eligible
    --image-filter name=~^docker-registry\.wikimedia\.org/

    # Images with label "gc" with value yes"
    --image-filter label:gc==yes

Filters are OR'd together, meaning that if any one of then matches,
the resource is considered eligible for pruning.

Run `docker-gc.py --help` for usage details.

## Run locally

Shell scripts `run-resource-monitor.sh`, `run-docker-gc.sh` and
`run-docker-gc-dates.sh` can be used to run the tools locally
